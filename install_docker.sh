#!/bin/bash

# Mise à jour des paquets
sudo apt-get update

# Installation de Docker
sudo apt-get install -y docker.io

# Ajout de l'utilisateur vagrant au groupe docker
sudo usermod -aG docker vagrant

# Configuration pour permettre à l'utilisateur vagrant d'utiliser Docker sans sudo
sudo echo 'vagrant ALL=(ALL) NOPASSWD: /usr/bin/docker' | sudo tee -a /etc/sudoers.d/vagrant_docker
# Redémarrage de la machine
#sudo reboot